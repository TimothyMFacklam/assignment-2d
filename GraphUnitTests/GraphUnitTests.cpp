/*
* Assignment 2D: Timothy Facklam 2022
* Summary: Unit testing of the graph.h class. Format for test cases
* are Test_<FunctionName>_<UniqueParamaters/Test Circumstances>.
* Underscores surrounding the function name are purely for readability
* 
*/
#include "pch.h"
#include "CppUnitTest.h"
#include "../GraphExample/Graph.h"
#include <iostream>
#include <fstream>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace GraphUnitTests
{
	TEST_CLASS(GraphUnitTests)
	{
	public:
		/// <summary>
		/// Testing the vertex struct constructor to ensure it sets data correctly.
		/// </summary>
		TEST_METHOD(Test_VertexStruct_Constructor)
		{
			Graph<string>::Vertex Vertex{ "Vertex" };
			Assert::AreEqual((string)"Vertex", Vertex.info);
		}
		/// <summary>
		/// Testing the vertex struct AddEdge() function. Edge should be added to the
		/// adjacency list.
		/// </summary>
		TEST_METHOD(Test_VertexStruct_AddEdge)
		{
			Graph<string>::Vertex vertex{ "Vertex" };
			Graph<string>::Vertex EdgeVertex{ "EdgeVertex" };


			Graph<string>::Vertex::Edge edge = make_pair(500, &EdgeVertex);
			vertex.AddEdge(edge);
				Assert::AreEqual((string)"EdgeVertex", vertex.adjacency.begin()->second->info);
		}
		/// <summary>
		/// Testing the vertex struct RemoveEdge() function. Edge should be removed from the 
		/// adjacency list.
		/// </summary>
		TEST_METHOD(Test_VertexStruct_RemoveEdge)
		{
			Graph<string>::Vertex vertex{ "Vertex" };
			Graph<string>::Vertex EdgeVertex{ "EdgeVertex" };


			Graph<string>::Vertex::Edge edge = make_pair(500, &EdgeVertex);
			vertex.AddEdge(edge);
			Assert::AreEqual((string)"EdgeVertex", vertex.adjacency.begin()->second->info);
			vertex.RemoveEdge(edge);
			Assert::AreEqual(0, (int)vertex.adjacency.size());

		}
		/// <summary>
		/// Testing the LessThanOperator, <, when the first vertex is smaller. Should return 
		/// a boolean true. 
		/// </summary>
		TEST_METHOD(Test_LessThanOperator_Vertex1Smaller)
		{
			Graph<string>::Vertex vertex1{ "Vertex1" };
			Graph<string>::Vertex vertex2{ "Vertex2" };

			vertex1.dist = 1;
			vertex2.dist = 2;

			Assert::IsTrue(vertex1 < vertex2);
		}
		/// <summary>
		/// Testing the LessThenOperator, <, when the second vertex is smaller. Should return a
		/// boolean false.
		/// </summary>
		TEST_METHOD(Test_LessThanOperator_Vertex2Smaller)
		{
			Graph<string>::Vertex vertex1{ "Vertex1" };
			Graph<string>::Vertex vertex2{ "Vertex2" };

			vertex1.dist = 2;
			vertex2.dist = 1;

			Assert::IsFalse(vertex1 < vertex2);
		}
		/// <summary>
		/// Testing the LessThenOperator, <, when both vertices are the same distance. Should return a
		/// boolean false.
		/// </summary>
		TEST_METHOD(Test_LessThanOperator_VerticesSameDist)
		{
			Graph<string>::Vertex vertex1{ "Vertex1" };
			Graph<string>::Vertex vertex2{ "Vertex2" };

			vertex1.dist = 1;
			vertex2.dist = 1;

			Assert::IsFalse(vertex1 < vertex2);
		}
		/// <summary>
		/// Testing the GreaterThenOperator, >, when the firsts vertex is smaller. Should return a
		/// boolean false.
		/// </summary>
		TEST_METHOD(Test_GreaterThanOperator_Vertex1Smaller)
		{
			Graph<string>::Vertex vertex1{ "Vertex1" };
			Graph<string>::Vertex vertex2{ "Vertex2" };

			vertex1.dist = 1;
			vertex2.dist = 2;

			Assert::IsFalse(vertex1 > vertex2);
		}
		/// <summary>
		/// Testing the GreaterThenOperator, >, when the second vertex is smaller. Should return a
		/// boolean true.
		/// </summary>
		TEST_METHOD(Test_GreaterThanOperator_Vertex2Smaller)
		{
			Graph<string>::Vertex vertex1{ "Vertex1" };
			Graph<string>::Vertex vertex2{ "Vertex2" };

			vertex1.dist = 2;
			vertex2.dist = 1;

			Assert::IsTrue(vertex1 > vertex2);
		}
		/// <summary>
		/// Testing the GreaterThenOperator, >, when both vertices are the same distance. Should return a
		/// boolean false.
		/// </summary>
		TEST_METHOD(Test_GreaterThanOperator_VerticesSameDist)
		{
			Graph<string>::Vertex vertex1{ "Vertex1" };
			Graph<string>::Vertex vertex2{ "Vertex2" };

			vertex1.dist = 1;
			vertex2.dist = 1;

			Assert::IsFalse(vertex1 > vertex2);
		}
		/// <summary>
		/// Testing the CompareVStruct overloading the () operator when the RHS distance is
		/// greater than the LHS distance. Should return a boolean false.
		/// </summary>
		TEST_METHOD(Test_CompareVStruct_OperatorParenthesesOverload_RHSDistGreater)
		{
			Graph<string>::Vertex vertex1{ "Vertex1" };
			Graph<string>::Vertex vertex2{ "Vertex2" };
			vertex1.dist = 1;
			vertex2.dist = 2;

			Graph<string>::CompareV b;
			Assert::IsFalse(b(&vertex1, &vertex2));
		}
		/// <summary>
		/// Testing the CompareVStruct overloading the () operator when the RHS distance is
		/// less than the LHS distance. Should return a boolean true.
		/// </summary>
		TEST_METHOD(Test_CompareVStruct_OperatorParenthesesOverload_LHSDistGreater)
		{
			Graph<string>::Vertex vertex1{ "Vertex1" };
			Graph<string>::Vertex vertex2{ "Vertex2" };
			vertex1.dist = 2;
			vertex2.dist = 1;

			Graph<string>::CompareV b;
			Assert::IsTrue(b(&vertex1, &vertex2));
		}
		/// <summary>
		/// Testing the CompareVStruct overloading the () operator when the RHS distance and 
		/// LHS are equal to each other. Should return a boolean false.
		/// </summary>
		TEST_METHOD(Test_CompareVStruct_OperatorParenthesesOverload_LHSAndRHSEqual)
		{
			Graph<string>::Vertex vertex1{ "Vertex1" };
			Graph<string>::Vertex vertex2{ "Vertex2" };
			vertex1.dist = 1;
			vertex2.dist = 1;
			Graph<string>::CompareV b;
			Assert::IsFalse(b(&vertex1, &vertex2));
		}
		/// <summary>
		/// Testing the Vertices() function to ensure that it returns a list of
		/// the vertices within the graph. Vertices added should be listed
		/// within the list returned.
		/// </summary>
		TEST_METHOD(Test_Vertices_WithVertices)
		{
			Graph<string> G;
			G.AddVertex("Test Data");
			Assert::AreEqual(1, (int)G.Vertices().size());
			Assert::AreEqual((string)("Test Data"), G.Vertices().at(0)->info);
		}
		/// <summary>
		/// Testing the Vertices() function to ensure that it returns a list of
		/// the vertices within the graph. We are checking to see what the list returns
		/// when no vertices have been added. The list should have no items within it
		/// </summary>
		TEST_METHOD(Test_Vertices_WithoutVertices)
		{
			Graph<string> G;
			Assert::AreEqual(0, (int)G.Vertices().size());
		}
		/// <summary>
		/// Testing the AddVertex() function ensure the basic functionality of adding 
		/// a vertex is valid. An vertex should be added to the graph.
		/// </summary>
		TEST_METHOD(Test_AddVertex_ValidVertex)
		{
			Graph<string> G;
			G.AddVertex("Test Data");
			Assert::AreEqual(1, (int)G.Vertices().size());
			Assert::AreEqual((string)("Test Data"), G.Vertices().at(0)->info);
		}
		/// <summary>
		/// Testing the AddVertex() function for adding a vertex that has 
		/// already been added. Vertex should not be added to the
		/// list
		/// </summary>
		TEST_METHOD(Test_AddVertex_SameVertex)
		{
			Graph<string> G;
			G.AddVertex("Test Data");
			Assert::AreEqual(1, (int)G.Vertices().size());
			Assert::AreEqual((string)("Test Data"), G.Vertices().at(0)->info);

			G.AddVertex("Test Data");
			//Vertex is not added, thus size = 1
			Assert::AreEqual(1, (int)G.Vertices().size());
		}
		/// <summary>
		/// Testing the RemoveVertex() function when deleting a vertex without edges. 
		/// Vertex should be deleted
		/// </summary>
		TEST_METHOD(Test_RemoveVertex_WithoutEdges)
		{
			Graph<string> G;
			G.AddVertex("Test Data");
			Assert::AreEqual(1, (int)G.Vertices().size());
			Assert::AreEqual((string)("Test Data"), G.Vertices().at(0)->info);

			G.RemoveVertex("Test Data");
			Assert::AreEqual(0, (int)G.Vertices().size());
		}
		/// <summary>
		/// Testing the RemoveVertex() function when deleting a vertex with edges. 
		/// Vertex should be deleted with any attached edges within other vertices.
		/// </summary>
		TEST_METHOD(Test_RemoveVertex_WithEdges)
		{
			Graph<string> G;
	
			G.AddEdge("Test Data1", "Test Data2", 600);
			Assert::AreEqual(1, (int)G.Vertices().at(0)->adjacency.size());
			G.RemoveVertex("Test Data2");

			//Edge no longer exists from TestData1 to TestData2, thus the adjacency list 
			//should be equal to zero
			Assert::AreEqual(0, (int)G.Vertices().at(0)->adjacency.size());
			Assert::AreEqual(1, (int)G.Vertices().size());
		}
		/// <summary>
		/// Testing the RemoveVertex() function when deleting a vertex that is not
		/// currently within the graph. No edges or vertices should be deleted
		/// </summary>
		TEST_METHOD(Test_RemoveVertex_NotInGraph)
		{
			Graph<string> G;
			G.AddEdge("Vertex1", "Vertex2", 500);
			Assert::AreEqual(2, (int)G.Vertices().size());
			Assert::AreEqual((string)("Vertex1"), G.Vertices().at(0)->info);
			Assert::AreEqual((string)("Vertex2"), G.Vertices().at(1)->info);
			Assert::AreEqual((string)("Vertex2"), G.Vertices().at(0)->adjacency.begin()->second->info);

			G.RemoveVertex("Test Data2");
			Assert::AreEqual((string)("Vertex1"), G.Vertices().at(0)->info);
			Assert::AreEqual((string)("Vertex2"), G.Vertices().at(1)->info);
			Assert::AreEqual((string)("Vertex2"), G.Vertices().at(0)->adjacency.begin()->second->info);
		}
		/// <summary>
		/// Testing the AddEdge() function when adding an edge with a from destination and to 
		/// destination. Edge should be added.
		/// </summary>
		TEST_METHOD(Test_AddEdge_ValidVertices)
		{
			Graph<string> G;
			G.AddVertex("Test Data1");
			Assert::AreEqual(1, (int)G.Vertices().size());
			Assert::AreEqual((string)("Test Data1"), G.Vertices().at(0)->info);
		
			G.AddVertex("Test Data2");
			Assert::AreEqual((string)("Test Data2"), G.Vertices().at(1)->info);

			G.AddEdge("Test Data1", "Test Data2", 600);
			Assert::AreEqual(600, (int)G.Vertices().at(0)->adjacency.begin()->first);
			Assert::AreEqual(G.Vertices().at(0)->adjacency.begin()->second->info, G.Vertices().at(1)->info);
			Assert::AreEqual((double)600, G.Vertices().at(0)->adjacency.begin()->first);
		}
		/// <summary>
		/// Testing the AddEdge() function when adding an edge without either of the entered destinations 
		/// being entered into the graph. Both vertices will be added to the graph.
		/// </summary>
		TEST_METHOD(Test_AddEdge_WithNoValidVertices)
		{
			Graph<string> G;
			G.AddVertex("Test Data1");
			Assert::AreEqual((string)("Test Data1"), G.Vertices().at(0)->info);

			G.AddVertex("Test Data2");
			Assert::AreEqual((string)("Test Data2"), G.Vertices().at(1)->info);
			
			Assert::AreEqual(2, (int)G.Vertices().size());


			G.AddEdge("Test Data3", "Test Data4", 600);
			Assert::AreEqual(4, (int)G.Vertices().size());
			Assert::AreEqual((string)("Test Data3"), G.Vertices().at(2)->info);
			Assert::AreEqual((string)("Test Data4"), G.Vertices().at(3)->info);
			Assert::AreEqual((string)("Test Data4"), G.Vertices().at(2)->adjacency.begin()->second->info);
			Assert::AreEqual((double)600, G.Vertices().at(2)->adjacency.begin()->first);
		}
		/// <summary>
		/// Testing the AddEdge() function when adding an edge without the first vertex being entered 
		/// within the graph. First destination vertex will be added to the graph.
		/// </summary>
		TEST_METHOD(Test_AddEdge_WithInvalidFirstVertex)
		{
			Graph<string> G;
			G.AddVertex("Test Data1");
			Assert::AreEqual(1, (int)G.Vertices().size());
			Assert::AreEqual((string)("Test Data1"), G.Vertices().at(0)->info);

			G.AddVertex("Test Data2");
			Assert::AreEqual((string)("Test Data2"), G.Vertices().at(1)->info);

			G.AddEdge("Test Data3", "Test Data2", 600);
			Assert::AreEqual(3, (int)G.Vertices().size());
			Assert::AreEqual((string)("Test Data3"), G.Vertices().at(2)->info);
			Assert::AreEqual((string)("Test Data2"), G.Vertices().at(2)->adjacency.begin()->second->info);
			Assert::AreEqual((double)600, G.Vertices().at(2)->adjacency.begin()->first);
		}
		/// <summary>
		/// Testing the AddEdge() function when adding an edge without the second vertex being entered 
		/// within the graph. The second vertex will be added to the graph.
		/// </summary>
		TEST_METHOD(Test_AddEdge_WithInvalidLastVertex)
		{
			Graph<string> G;
			G.AddVertex("Test Data1");
			Assert::AreEqual(1, (int)G.Vertices().size());
			Assert::AreEqual((string)("Test Data1"), G.Vertices().at(0)->info);

			G.AddVertex("Test Data2");
			Assert::AreEqual((string)("Test Data2"), G.Vertices().at(1)->info);

			G.AddEdge("Test Data1", "Test Data4", 600);
			Assert::AreEqual(3, (int)G.Vertices().size());
			Assert::AreEqual((string)("Test Data4"), G.Vertices().at(2)->info);
			Assert::AreEqual((string)("Test Data4"), G.Vertices().at(0)->adjacency.begin()->second->info);
			Assert::AreEqual((double)600, G.Vertices().at(0)->adjacency.begin()->first);
		}
		/// <summary>
		/// Testing the RemoveEdge() function when removing an edge that already exists within a graph 
		/// given the proper weight and the two vertices it connects to. Edge will be deleted.
		/// </summary>
		TEST_METHOD(Test_RemoveEdge_WithValidEdgesAndWeight)
		{
			Graph<string> G;

			G.AddEdge("Test Data1", "Test Data2", 600);
			G.RemoveEdge("Test Data1", "Test Data2", 600);

			Assert::AreEqual(0, (int)(G.Vertices().at(0)->adjacency.size()));

		}
		/// <summary>
		/// Testing the RemoveEdge() function when the first vertex and
		/// an improper distance are used as parameters. Edge should not be removed
		/// </summary>
		TEST_METHOD(Test_RemoveEdge_WithInvalidFirstVertexImproperWeight)
		{
			Graph<string> G;

			G.AddEdge("Test Data1", "Test Data2", 600);
			G.RemoveEdge("Test Data3", "Test Data2", 400);
			Assert::AreEqual(1, (int)(G.Vertices().at(0)->adjacency.size()));
			Assert::AreEqual((string)("Test Data2"), G.Vertices().at(0)->adjacency.begin()->second->info);
		}
		/// <summary>
		/// Testing the RemoveEdge() function when both entered vertices are 
		/// invalid and the distance is incorrect. Should not delete the edge.
		/// </summary>
		TEST_METHOD(Test_RemoveEdge_WithInvalidVerticesImproperWeight)
		{
			Graph<string> G;
			G.AddEdge("Test Data1", "Test Data2", 600);

			G.RemoveEdge("Test Data3", "Test Data4", 22);
			Assert::AreEqual(1, (int)(G.Vertices().at(0)->adjacency.size()));
			Assert::AreEqual((string)("Test Data2"), G.Vertices().at(0)->adjacency.begin()->second->info);
		}
		/// <summary>
		/// Testing the RemoveEdge() function when an invalid first vertex
		/// but valid second vertex and valid distance works. Should not remove the
		/// edge.
		/// </summary>
		TEST_METHOD(Test_RemoveEdge_WithInvalidFirstVertexValidWeight)
		{
			Graph<string> G;
			G.AddEdge("Test Data1", "Test Data2", 600);

			G.RemoveEdge("Test Data4", "Test Data2", 600);
			Assert::AreEqual(1, (int)(G.Vertices().at(0)->adjacency.size()));
			Assert::AreEqual((string)("Test Data2"), G.Vertices().at(0)->adjacency.begin()->second->info);
		}
		/// <summary>
		/// Testing the RemoveEdge() function when the second vertex 
		/// parameter is entered incorrectly but a valid distance and 
		/// first vertex is entered. Should not remove the edge
		/// </summary>
		TEST_METHOD(Test_RemoveEdge_WithInvalidSecondVertexValidWeight)
		{
			Graph<string> G;
			G.AddVertex("Test Data1");
			G.AddVertex("Test Data2");
			
			G.AddEdge("Test Data1", "Test Data2", 600);
			G.RemoveEdge("Test Data1", "Test Data4", 600);
			Assert::AreEqual(1, (int)(G.Vertices().at(0)->adjacency.size()));
			Assert::AreEqual((string)("Test Data2"), G.Vertices().at(0)->adjacency.begin()->second->info);
		}
		/// <summary>
		/// Testing the RemoveEdge() function when improper vertices 
		/// but valid distance is entered. Edge should not be removed
		/// </summary>
		TEST_METHOD(Test_RemoveEdge_WithInvalidVerticesValidDistance)
		{
			Graph<string> G;
			G.AddVertex("Test Data1");
			G.AddVertex("Test Data2");

			G.AddEdge("Test Data1", "Test Data2", 600);
			G.RemoveEdge("Test Data3", "Test Data4", 600);
			Assert::AreEqual(1, (int)(G.Vertices().at(0)->adjacency.size()));
			Assert::AreEqual((string)("Test Data2"), G.Vertices().at(0)->adjacency.begin()->second->info);
		}
		/// <summary>
		/// Testing DepthFirstSeach() function when the first vertex is not valid
		/// Should return a boolean false.
		/// </summary>
		TEST_METHOD(Test_DepthFirstSearchPath_FirstVertexNotValid)
		{
			Graph<string> G;
			deque<Graph<string>::Vertex*> path;
			G.AddEdge("Test Data1", "Test Data2", 600);
			
			Assert::IsFalse(G.DepthFirstSearch("Invalid", "Test Data2", path));
			Assert::IsTrue(path.size() == 0);
		}
		/// <summary>
		/// Testing DepthFirstSeach() function when the second vertex is not valid
		/// Should return a boolean false.
		/// </summary>
		TEST_METHOD(Test_DepthFirstSearchPath_SecondVertexNotValid)
		{
			Graph<string> G;
			deque<Graph<string>::Vertex*> path;
			G.AddEdge("Test Data1", "Test Data2", 600);

			Assert::IsFalse(G.DepthFirstSearch("Test Data1", "Invalid", path));
			Assert::IsTrue(path.size() == 0);
		}
		/// <summary>
		/// CTesting DepthFirstSeach() function when both vertices are not valid.
		/// Should return a boolean false.
		/// </summary>
		TEST_METHOD(Test_DepthFirstSearchPath_BothVerticesNotValid)
		{
			Graph<string> G;
			deque<Graph<string>::Vertex*> path;
			G.AddEdge("Test Data1", "Test Data2", 600);

			Assert::IsFalse(G.DepthFirstSearch("Invalid", "Invalid", path));
			Assert::IsTrue(path.size() == 0);
		}
		/// <summary>
		/// Testing the DepthFirstSearch() function when a path does not exist from the
		/// first vertex to the second vertex. The function should return a boolean
		/// false since a path cannot be found. 
		/// </summary>
		TEST_METHOD(Test_DepthFirstSearch_PathNotFound)
		{
			Graph<string> G;
			deque<Graph<string>::Vertex*> path;
			G.AddEdge("Test Data1", "Test Data2", 600);

			Assert::IsFalse(G.DepthFirstSearch("Test Data2", "Test Data1", path));
		}
		/// <summary>
		/// Testing the DepthFirstSearch() function when a valid path exists and 
		/// is found. Should return a boolean true. It should also follow the depthfirst search 
		/// algorithm. The graph example I provide has one node that leads to the destination with only
		/// two steps(that is 1 to 5 to 4) while the DFS should take the longest path being 1 to 2 to 3 to 4.
		///        ------ > 5 ----> 
		///      1 - > 2 - > 3 - > 4
		///
		/// </summary>
		TEST_METHOD(Test_DepthFirstSearch_PathFound)
		{
			Graph<string> G;
			deque<Graph<string>::Vertex*> path;
			G.AddEdge("1", "5", 100);
			G.AddEdge("5", "4", 100);
			G.AddEdge("1", "2", 100);
			G.AddEdge("2", "3", 100);
			G.AddEdge("3", "4", 100);

			Assert::IsTrue(G.DepthFirstSearch("1", "4", path));

			Assert::AreEqual((string)"1", path.at(0)->info);
			Assert::AreEqual((string)"2", path.at(1)->info);
			Assert::AreEqual((string)"3", path.at(2)->info);
			Assert::AreEqual((string)"4", path.at(3)->info);
		}
		/// <summary>
		/// Testing the BreadthFirstSearch when both vertices are not within the
		/// graph. The function should return a boolean false.
		/// </summary>
		TEST_METHOD(Test_BreadthFirstSearchPath_BothVerticesNotValid)
		{
			Graph<string> G;
			deque<Graph<string>::Vertex*> path;
			G.AddEdge("Test Data1", "Test Data2", 600);


			Assert::IsFalse(G.BreadthFirstSearch("Invalid", "Invalid", path));
			Assert::IsTrue(path.size() == 0);
		}
		/// <summary>
		/// Testing the BreadthFirstSearch when the first vertex entered is not
		/// a valid vertex. The function should return a boolean false in this
		/// test case.
		/// </summary>
		TEST_METHOD(Test_BreadthFirstSearchPath_FirstVertexNotValid)
		{
			Graph<string> G;
			deque<Graph<string>::Vertex*> path;
			G.AddEdge("Test Data1", "Test Data2", 600);

			Assert::IsFalse(G.BreadthFirstSearch("Invalid", "Test Data2", path));
			Assert::IsTrue(path.size() == 0);
		
		}
		/// <summary>
		/// Testing the BreadthFirstSearch when the second vertex entered 
		/// is not a valid vertex. The function should return a boolean false
		/// in this test case.
		/// </summary>
		TEST_METHOD(Test_BreadthFirstSearchPath_SecondVertexNotValid)
		{
			Graph<string> G;
			deque<Graph<string>::Vertex*> path;
			G.AddEdge("Test Data1", "Test Data2", 600);

			Assert::IsFalse(G.BreadthFirstSearch("Test Data1", "Invalid", path));
			Assert::IsTrue(path.size() == 0);
		}
		/// <summary>
		/// Testing the BreadthFirstSearch() function when two valid vertices are 
		/// entered as parameters, but no valid path exits. Should return a
		/// boolean false.
		/// /// </summary>
		TEST_METHOD(Test_BreadthFirstSearch_PathNotFound)
		{
			Graph<string> G;
			deque<Graph<string>::Vertex*> path;
			G.AddEdge("Test Data1", "Test Data2", 600);

			Assert::IsFalse(G.BreadthFirstSearch("Test Data2", "Test Data1", path));
			Assert::IsTrue(path.size() == 0);
		}
		/// <summary>
		/// Tesing the BreadthFirstSearch() function when a valid path is found. It 
		/// should return a boolean false. Path will also provide a backtracked version
		/// of BFS using what ever node discoved the node in a path as the prev. 
		/// The example graph I used is 
		/// 
		///  1 -> 2 - > 7
		///	\ 
		///	  > 3 - > 4 - > 5
		///					 \ 
		///						> 6
		/// 
		/// </summary>
		TEST_METHOD(Test_BreadthFirstSearch_PathFound)
		{
			Graph<string> G;
			deque<Graph<string>::Vertex*> path;
			
			G.AddEdge("1", "2", 100);
			G.AddEdge("1", "3", 100);
			G.AddEdge("2", "7", 100);
			G.AddEdge("3", "4", 100);
			G.AddEdge("4", "6", 100);
			G.AddEdge("4", "5", 100);


			Assert::IsTrue(G.BreadthFirstSearch("1", "6", path));

			//BreadthFirstSearch returns the path from the last node discovered by
			//setting the prev of a node, to the node you came from to discover the
			//new node.
			Assert::AreEqual((string)"1", path.at(0)->info);
			Assert::AreEqual((string)"3", path.at(1)->info);
			Assert::AreEqual((string)"4", path.at(2)->info);
			Assert::AreEqual((string)"6", path.at(3)->info);
		}
		/// <summary>
		/// Testing the Print() function when there are neither edges or
		/// vertices within the graph. Vertices should display how many edges 
		/// they have. They should also display which edges they have below them
		/// accompanied with the respective distance. There should be no edges 
		/// or vertices displayed in the test.
		/// </summary>
		TEST_METHOD(Test_Print_NoEdgesNoVertices)
		{
			Graph<string> G;
			ofstream outFile;
			outFile.open("sample.txt");

			G.Print(outFile);
			if (!outFile.is_open())
			{
				Assert::IsTrue(false);
			}
			outFile.close();
			ifstream inFile;
			inFile.open("sample.txt");
			string data;
			inFile >> data;
			Assert::AreEqual((string)"", data);
		}
		/// <summary>
		/// Testing the Print() function when there are both edges and 
		/// vertices within the graph. Vertices should display how many edges 
		/// they have. They should also display which edges they have below them
		/// accompanied with the respective distance.
		/// </summary>
		TEST_METHOD(Test_Print_EdgesAndVertices)
		{
			ofstream outFile;
			ifstream inFile;
			string data;
			Graph<string> G;

			G.AddEdge("Vertex1", "Vertex2", 500);

			outFile.open("sample.txt");
			if (!outFile.is_open())
			{
				Assert::IsTrue(false);
			}

			G.Print(outFile);
			outFile.close();
			inFile.open("sample.txt");
			
			inFile >> data;
			Assert::AreEqual((string)"Vertex1", data);
			inFile >> data;
			Assert::AreEqual((string)"1", data);
			inFile >> data;
			Assert::AreEqual((string)"500", data);
			inFile >> data;
			Assert::AreEqual((string)"Vertex2", data);
			inFile >> data;
			Assert::AreEqual((string)"Vertex2", data);
			inFile >> data;
			Assert::AreEqual((string)"0", data);
		}
		/// <summary>
		/// Testing the Print() function when there are no edges but has
		/// vertices within the graph. Vertices should display how many edges 
		/// they have. They should also display which edges they have below them
		/// accompanied with the respective distance. In this test, vertices should 
		/// display, but should have no edges 
		/// </summary>
		TEST_METHOD(Test_Print_VerticesNoEdges)
		{
			ofstream outFile;
			ifstream inFile;
			string data;
			Graph<string> G;
			G.AddVertex("Vertex1");
			G.AddVertex("Vertex2");

			outFile.open("sample.txt");
			if (!outFile.is_open())
			{
				Assert::IsTrue(false);
			}

			G.Print(outFile);
			outFile.close();
			inFile.open("sample.txt");

			inFile >> data;
			Assert::AreEqual((string)"Vertex1", data);
			inFile >> data;
			Assert::AreEqual((string)"0", data);
			inFile >> data;
			Assert::AreEqual((string)"Vertex2", data);
			inFile >> data;
			Assert::AreEqual((string)"0", data);
		}
		/// <summary>
		/// Testing the PrintPath() function with parameters for the shortest path
		/// and an invalid first vertex. 
		/// </summary>
		TEST_METHOD(Test_PrintPath_ShortestPath_InvalidFirstVertex)
		{
			ofstream outFile;
			ifstream inFile;
			string data;
			Graph<string> G;

			G.AddEdge("Vertex1", "Vertex2", 500);

			outFile.open("sample.txt");
			if (!outFile.is_open())
			{
				Assert::IsTrue(false);
			}

			try
			{
				G.PrintPath(outFile, "Invalid", "Vertex2", 's');
				Assert::IsTrue(true);
			}
			catch(...)
			{
				Assert::IsTrue(false);
			}

			outFile.close();
		}
		/// <summary>
		/// Testing the PrintPath() function with parameters for the shortest path
		/// and an invalid second vertex. 
		/// </summary>
		TEST_METHOD(Test_PrintPath_ShortestPath_InvalidSecondVertex)
		{
			ofstream outFile;
			ifstream inFile;
			string data;
			Graph<string> G;

			G.AddEdge("Vertex1", "Vertex2", 500);

			outFile.open("sample.txt");
			if (!outFile.is_open())
			{
				Assert::IsTrue(false);
			}

			try
			{
				G.PrintPath(outFile, "Vertex1", "Invalid", 's');
				Assert::IsTrue(true);
			}
			catch (...)
			{
				Assert::IsTrue(false);
			}

			outFile.close();
		}
		/// <summary>
		/// Testing the PrintPath() function with parameters for the shortest path
		/// and an invalid vertices. 
		/// </summary>
		TEST_METHOD(Test_PrintPath_ShortestPath_InvalidVertices)
		{
			ofstream outFile;
			ifstream inFile;
			string data;
			Graph<string> G;

			G.AddEdge("Vertex1", "Vertex2", 500);

			outFile.open("sample.txt");
			if (!outFile.is_open())
			{
				Assert::IsTrue(false);
			}

			try
			{
				G.PrintPath(outFile, "Invalid", "Invalid", 's');
				Assert::IsTrue(true);
			}
			catch (...)
			{
				Assert::IsTrue(false);
			}

			outFile.close();
		}
		/// <summary>
		/// Testing the PrintPath() function with parameters for the shortest path
		/// with valid vertices but no valid path. Reading from output file should
		/// return with "Cannot find a path!".
		/// </summary>
		TEST_METHOD(Test_PrintPath_ShortestPath_PathNotFound)
		{
			ofstream outFile;
			ifstream inFile;
			string data;
			Graph<string> G;

			G.AddEdge("Vertex1", "Vertex2", 500);

			outFile.open("sample.txt");
			if (!outFile.is_open())
			{
				Assert::IsTrue(false);
			}

			G.PrintPath(outFile, "Vertex2", "Vertex1", 's');
			outFile.close();
			inFile.open("sample.txt");
			while (inFile)
			{
				string b;
				inFile >> b;
				if (data != "" && b != "")
					data = data + " " + b;
				else if (b != "")
					data = b;
			}
			Assert::AreEqual((string)"Cannot find a path!", data);
		}
		/// <summary>
		/// Testing the PrintPath() function with the parameters for the shortest path
		/// function. A valid path exists. Reading from output should display the path and the 
		/// distance from each node in that path.
		/// </summary>
		TEST_METHOD(Test_PrintPath_ShortestPath_PathFound)
		{
			ofstream outFile;
			ifstream inFile;
			string data;
			Graph<string> G;


			//Shortest path returns the shortest path based on the amount of nodes in the
			//path


			G.AddEdge("1", "3", 1);
			G.AddEdge("3", "4", 1);
			G.AddEdge("5", "6", 1);
			G.AddEdge("1", "6", 1000);
			G.AddEdge("4", "6", 1);
			G.AddEdge("1", "7", 1);
			G.AddEdge("7", "6", 100);

			outFile.open("sample.txt");
			if (!outFile.is_open())
			{
				Assert::IsTrue(false);
			}

			G.PrintPath(outFile, "1", "6", 's');
			outFile.close();
			inFile.open("sample.txt");
			string b;
			while (inFile)
			{
				inFile >> b;
				if (data != "" && b != "")
					data = data + " " + b;
				else if (b != "")
					data = b;
			}
			//Node 1
			Assert::AreEqual((string)"1", G.Vertices().at(0)->prev->info);
			//Node 3
			Assert::AreEqual((string)"1", G.Vertices().at(1)->prev->info);
			//Node 4
			Assert::AreEqual((string)"3", G.Vertices().at(2)->prev->info);
			//Node 5 prev is Null since it is not connected from any nodes starting from 1
			Assert::IsNull(G.Vertices().at(3)->prev);
			//Node 6
			Assert::AreEqual((string)"1", G.Vertices().at(4)->prev->info);
			//Node 7
			Assert::AreEqual((string)"1", G.Vertices().at(5)->prev->info);

			Assert::AreEqual((string)"1-->6-->DONE!", b);
		}
		/// <summary>
		/// Testing the PrintPath() function with parametes for the DFS with an
		/// invalid first vertex. Reading from output file should return with 
		/// "Cannot find a path!".
		/// </summary>
		TEST_METHOD(Test_PrintPath_DepthFirstSearch_InvalidFirstVertex)
		{
			ofstream outFile;
			ifstream inFile;
			string data;
			Graph<string> G;

			G.AddEdge("Vertex1", "Vertex2", 500);

			outFile.open("sample.txt");
			if (!outFile.is_open())
			{
				Assert::IsTrue(false);
			}

			G.PrintPath(outFile, "Invalid", "Vertex2", 'd');
			outFile.close();
			inFile.open("sample.txt");
			while (inFile)
			{
				string b;
				inFile >> b;
				if (data != "" && b != "")
					data = data + " " + b;
				else if(b != "")
					data = b;
			}
			Assert::AreEqual((string)"Cannot find a path!", data);
		}
		/// <summary>
		/// Testing the PrintPath() function with parametes for the DFS with an
		/// invalid second vertex. Reading from output file should return with 
		/// "Cannot find a path!".
		/// </summary>
		TEST_METHOD(Test_PrintPath_DepthFirstSearch_InvalidSecondVertex)
		{
			ofstream outFile;
			ifstream inFile;
			string data;
			Graph<string> G;

			G.AddEdge("Vertex1", "Vertex2", 500);

			outFile.open("sample.txt");
			if (!outFile.is_open())
			{
				Assert::IsTrue(false);
			}

			G.PrintPath(outFile, "Vertex1", "Invalid", 'd');
			outFile.close();
			inFile.open("sample.txt");
			while (inFile)
			{
				string b;
				inFile >> b;
				if (data != "" && b != "")
					data = data + " " + b;
				else if (b != "")
					data = b;
			}
			Assert::AreEqual((string)"Cannot find a path!", data);
		}
		/// <summary>
		/// Testing the PrintPath() function with parametes for the DFS with an
		/// invalid vertices. Reading from output file should return with 
		/// "Cannot find a path!".
		/// </summary>
		TEST_METHOD(Test_PrintPath_DepthFirstSearch_InvalidVertices)
		{
			ofstream outFile;
			ifstream inFile;
			string data;
			Graph<string> G;

			G.AddEdge("Vertex1", "Vertex2", 500);

			outFile.open("sample.txt");
			if (!outFile.is_open())
			{
				Assert::IsTrue(false);
			}

			G.PrintPath(outFile, "Invalid", "Invalid", 'd');
			outFile.close();
			inFile.open("sample.txt");
			while (inFile)
			{
				string b;
				inFile >> b;
				if (data != "" && b != "")
					data = data + " " + b;
				else if (b != "")
					data = b;
			}
			Assert::AreEqual((string)"Cannot find a path!", data);
		}
		/// <summary>
		/// Testing the PrintPath() function with parameters for the DFS with an
		/// valid vertices but no traversable path. Reading from output file 
		/// should return with "Cannot find a path!".
		/// </summary>
		TEST_METHOD(Test_PrintPath_DepthFirstSearch_PathNotFound)
		{
			ofstream outFile;
			ifstream inFile;
			string data;
			Graph<string> G;

			G.AddEdge("Vertex1", "Vertex2", 500);

			outFile.open("sample.txt");
			if (!outFile.is_open())
			{
				Assert::IsTrue(false);
			}

			G.PrintPath(outFile, "Vertex2", "Vertex1", 'd');
			outFile.close();
			inFile.open("sample.txt");
			while (inFile)
			{
				string b;
				inFile >> b;
				if (data != "" && b != "")
					data = data + " " + b;
				else if (b != "")
					data = b;
			}
			Assert::AreEqual((string)"Cannot find a path!", data);
		}
		/// <summary>
		/// Testing the PrintPath() function with with DFS and valid vertices with a path 
		/// between them. Reading from output file should return with "1-->2-->3-->4-->DONE!".
		/// The example graph I use.
		///       
		///		    -- > 5 ----\
		///        /             >
		///       1 - > 2 - > 3 - > 4
		///
		/// </summary>
		TEST_METHOD(Test_PrintPath_DepthFirstSearch_PathFound)
		{
			ofstream outFile;
			ifstream inFile;
			string data;
			Graph<string> G;

			G.AddEdge("1", "5", 100);
			G.AddEdge("5", "4", 100);
			G.AddEdge("1", "2", 100);
			G.AddEdge("2", "3", 100);
			G.AddEdge("3", "4", 100);

			outFile.open("sample.txt");
			if (!outFile.is_open())
			{
				Assert::IsTrue(false);
			}

			G.PrintPath(outFile, "1", "4", 'd');
			outFile.close();
			inFile.open("sample.txt");
			string b;
			while (inFile)
			{
				inFile >> b;
				if (data != "" && b != "")
					data = data + " " + b;
				else if (b != "")
					data = b;
			}
			Assert::AreEqual((string)"1-->2-->3-->4-->DONE!", b);
		}
		/// <summary>
		/// Testing the PrintPath() function with parametes for the BFS with an
		/// invalid first vertex. Reading from output file should return with 
		/// "Cannot find a path!".
		/// </summary>
		TEST_METHOD(Test_PrintPath_BreadthFirstSearch_InvalidFirstVertex)
		{
			ofstream outFile;
			ifstream inFile;
			string data;
			Graph<string> G;

			G.AddEdge("Vertex1", "Vertex2", 500);

			outFile.open("sample.txt");
			if (!outFile.is_open())
			{
				Assert::IsTrue(false);
			}

			G.PrintPath(outFile, "Invalid", "Vertex2", 'b');
			outFile.close();
			inFile.open("sample.txt");
			while (inFile)
			{
				string b;
				inFile >> b;
				if (data != "" && b != "")
					data = data + " " + b;
				else if (b != "")
					data = b;
			}
			Assert::AreEqual((string)"Cannot find a path!", data);
		}
		/// <summary>
		/// Testing the PrintPath() function with parametes for the BFS with an
		/// invalid second vertex. Reading from output file should return with 
		/// "Cannot find a path!".
		/// </summary>
		TEST_METHOD(Test_PrintPath_BreadthFirstSearch_InvalidSecondVertex)
		{
			ofstream outFile;
			ifstream inFile;
			string data;
			Graph<string> G;

			G.AddEdge("Vertex1", "Vertex2", 500);

			outFile.open("sample.txt");
			if (!outFile.is_open())
			{
				Assert::IsTrue(false);
			}

			G.PrintPath(outFile, "Vertex1", "Invalid", 'b');
			outFile.close();
			inFile.open("sample.txt");
			while (inFile)
			{
				string b;
				inFile >> b;
				if (data != "" && b != "")
					data = data + " " + b;
				else if (b != "")
					data = b;
			}
			Assert::AreEqual((string)"Cannot find a path!", data);
		}
		/// <summary>
		/// Testing the PrintPath() function with parametes for the BFS with
		/// invalid vertices. Reading from output file should return with 
		/// "Cannot find a path!".
		/// </summary>
		TEST_METHOD(Test_PrintPath_BreadthFirstSearch_InvalidVertices)
		{
			ofstream outFile;
			ifstream inFile;
			string data;
			Graph<string> G;

			G.AddEdge("Vertex1", "Vertex2", 500);

			outFile.open("sample.txt");
			if (!outFile.is_open())
			{
				Assert::IsTrue(false);
			}

			G.PrintPath(outFile, "Invalid", "Invalid", 'b');
			outFile.close();
			inFile.open("sample.txt");
			while (inFile)
			{
				string b;
				inFile >> b;
				if (data != "" && b != "")
					data = data + " " + b;
				else if (b != "")
					data = b;
			}
			Assert::AreEqual((string)"Cannot find a path!", data);
		}
		/// <summary>
		/// Testing the PrintPath() function with parametes for the BFS with 
		/// valid vertices but no traversable path. Reading from output file 
		/// should return "Cannot find a path!".
		/// </summary>
		TEST_METHOD(Test_PrintPath_BreadthFirstSearch_PathNotFound)
		{
			ofstream outFile;
			ifstream inFile;
			string data;
			Graph<string> G;

			G.AddEdge("Vertex1", "Vertex2", 500);

			outFile.open("sample.txt");
			if (!outFile.is_open())
			{
				Assert::IsTrue(false);
			}

			G.PrintPath(outFile, "Vertex2", "Vertex1", 'b');
			outFile.close();
			inFile.open("sample.txt");
			while (inFile)
			{
				string b;
				inFile >> b;
				if (data != "" && b != "")
					data = data + " " + b;
				else if (b != "")
					data = b;
			}
			Assert::AreEqual((string)"Cannot find a path!", data);
		}
		/// <summary>
		/// Testing the PrintPath() function with with BFS and valid vertices with a path 
		/// between them. Reading from output file should return with "1-->3-->4-->6-->DONE!".
		/// The example graph I use.
		/// 		/// 
		///  1 -> 2 - > 7
		///	\ 
		///	  > 3 - > 4 - > 5
		///					 \ 
		///						> 6
		/// </summary>
		TEST_METHOD(Test_PrintPath_BreadthFirstSearch_PathFound)
		{
			ofstream outFile;
			ifstream inFile;
			string data;
			Graph<string> G;

			G.AddEdge("1", "2", 100);
			G.AddEdge("1", "3", 100);
			G.AddEdge("2", "7", 100);
			G.AddEdge("3", "4", 100);
			G.AddEdge("4", "6", 100);
			G.AddEdge("4", "5", 100);

			outFile.open("sample.txt");
			if (!outFile.is_open())
			{
				Assert::IsTrue(false);
			}

			G.PrintPath(outFile, "1", "6", 'b');
			outFile.close();
			inFile.open("sample.txt");
			string b;
			while (inFile)
			{
				inFile >> b;
				if (data != "" && b != "")
					data = data + " " + b;
				else if (b != "")
					data = b;
			}

			Assert::AreEqual((string)"1-->3-->4-->6-->DONE!", b);
		}
	};
}
